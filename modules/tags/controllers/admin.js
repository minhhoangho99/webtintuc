const {
    validationResult,
} = require('express-validator/check');
const TagRequests = require('../requests/admin');
const TagRepositories = require('../repositories/admin');
const HandleResponse = require('../../../helpers/handleResponse');
const Config = require('../../../config/config');
const Pagination = require('../../../helpers/pagination');

module.exports = {
    middlewareModify: async (req, res, next) => {
        const tagId = typeof req.params.id !== 'undefined' ? req.params.id : req.body.id;
        const userId = req.session.user.id;
        const tag = await TagRepositories.findTagById(tagId);
        if (!tag) return res.render('admin/body/error/404');
        if (tag.author.toString() !== userId && res.locals.cUser.role !== 'admin' && res.locals.cUser.role !== 'manager') {
            return res.render('admin/body/error/403');
        }
        next();
    },
    getAddTag: (req, res) => {
        res.render('admin/body/tag/add', {
            active: 'tag_add',
        });
    },
    addTag: async (req, res) => {
        try {
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = TagRequests.handleValidateTag(errors);
                return TagRequests.handleErrorAddTag(body, errMsg, res);
            }
            TagRepositories.saveTag(body).then(() => {
                req.flash('success', 'Thêm tag thành công');
                return TagRequests.handleSuccessTags('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    getEditTag: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            if (!id.match(/^[0-9a-fA-F]{24}$/)) {
                return HandleResponse.handleError(404, res);
            }
            const tag = await TagRepositories.findTagById(id);
            return TagRequests.renderEditTag(tag, res);
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    editTag: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = TagRequests.handleValidateTag(errors);
                return TagRequests.handleErrorEditTag(id, body, errMsg, res);
            }
            TagRepositories.updateTag(id, body).then(() => {
                req.flash('success', 'Sửa tag thành công');
                return TagRequests.handleSuccessTags('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    getTags: async (req, res) => {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await TagRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }

        const tags = await TagRepositories.getTagsOnPage(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/tag/manage');

        return TagRequests.handleSuccessTags('render', tags, pagination, res);
    },
    getTagsRecycleBin: async (req, res) => {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await TagRepositories.countDocuments({ deletedAt: { $ne: null } });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }

        const tags = await TagRepositories.getTagsOnBin(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/tag/bin');

        return TagRequests.handleDeleteTag('render', tags, pagination, res);
    },
    deleteTag: async (req, res) => {
        try {
            const { id } = req.body;
            const posts = await TagRepositories.findPostsByTags(id);
            let status;
            if (posts.length === 0) {
                req.flash('success', 'Xoá tag thành công');
                TagRepositories.updateTag(id, { deletedAt: Date.now() }).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                    throw error;
                });
            } else {
                await Promise.all(posts.map(async (e) => {
                    const { tags } = e;
                    if (tags.length === 1) {
                        req.flash('danger', 'Một số bài viết có chứa tag đã xoá. Vui lòng thay đổi tag cho bài viết hoặc xoá bài viết hoàn toàn trước khi xoá tag');
                        res.status(500).json({ message: 'error' });
                        status = 0;
                    }
                    return status;
                }));
                await Promise.all(posts.map(async (e) => {
                    const { tags } = e;
                    if (tags.length !== 1) {
                        tags.splice(tags.indexOf(id), 1);
                        await TagRepositories.updatePost(e._id, { tags });
                        status = 1;
                    }
                    return status;
                }));
                if (status === 0) return;
                if (status === 1) {
                    req.flash('success', 'Xoá tag thành công');
                    TagRepositories.updateTag(id, { deletedAt: Date.now() }).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                        throw error;
                    });
                }
            }
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    restoreTag: (req, res) => {
        try {
            const { id } = req.params;
            TagRepositories.updateTag(id, { deletedAt: null }).then(() => {
                req.flash('success', 'Khôi phục tag thành công');
                return TagRequests.handleSuccessTags('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    completedDeleteTag: (req, res) => {
        try {
            const { id } = req.body;
            req.flash('success', 'Xoá tag thành công');
            TagRepositories.deletedTag(id).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
};