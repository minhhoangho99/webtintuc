const express = require('express');
const ValidateHelpers = require('../../../helpers/validate');
const TagControllers = require('../controllers/admin');

const router = express.Router();

router.use(['/edit/:id', '/delete', '/bin/restore/:id', '/bin/completed-delete'], TagControllers.middlewareModify);

router.route('/add')
    .get(TagControllers.getAddTag)
    .post(ValidateHelpers.checkTag, TagControllers.addTag);
router.route('/edit/:id')
    .get(TagControllers.getEditTag)
    .post(ValidateHelpers.checkTag, TagControllers.editTag);
router.delete('/delete', TagControllers.deleteTag);
router.get(['/manage', '/manage/:page'], TagControllers.getTags);
router.get(['/bin', '/bin/:page'], TagControllers.getTagsRecycleBin);
router.get('/bin/restore/:id', TagControllers.restoreTag);
router.delete('/bin/completed-delete', TagControllers.completedDeleteTag);
module.exports = router;