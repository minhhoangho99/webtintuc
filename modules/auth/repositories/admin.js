const User = require('../../../models/user');

module.exports = {
    findUserById: id => User.findOne({
        _id: id,
        deletedAt: null,
    }).select('-__v -updatedAt'),
    findUser: (username, email) => User.findOne({
        $or: [{ username }, { email }],
        deletedAt: null,
    }).select('-__v -updatedAt'),
    findDeletedUser: (username, email) => User.findOne({
        $or: [{ username }, { email }],
        deletedAt: { $ne: null },
    }).select('-__v -updatedAt'),
    saveUser: (body) => {
        const {
            fullname,
            username,
            email,
            phoneNumber,
            password,
            role,
        } = body;

        const cUser = new User({
            fullname,
            username,
            email,
            phoneNumber,
            password,
            role,
        });
        return cUser.save();
    },
    updateUser: (id, field) => User.findByIdAndUpdate(id, field, {
        new: true,
    }),
};
