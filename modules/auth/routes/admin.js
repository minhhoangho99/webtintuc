const express = require('express');
const ValidateHelpers = require('../../../helpers/validate');
const AuthControllers = require('../controllers/admin');
const AuthRepositories = require('../repositories/admin');

const router = express.Router();

router.use(['/login', '/register'], (req, res, next) => {
    if (req.session.user) {
        return res.redirect('/admin');
    }
    return next();
});
router.route('/login')
    .get((req, res) => {
        res.render('admin/body/auth/login');
    })
    .post(AuthControllers.login);

router.route('/register')
    .get((req, res) => {
        res.render('admin/body/auth/register');
    })
    .post(ValidateHelpers.checkRegister, AuthControllers.register);

router.use(async (req, res, next) => {
    if (req.session.user) {
        if (!await AuthControllers.verifyUser(req.session.user)) {
            await req.session.destroy();
            const flash = { danger: ['Phiên làm việc hết hạn. Vui lòng đăng nhập lại'] };
            return res.render('admin/body/auth/login', { flash });
        }
        res.locals.cUser = req.session.user;
        return next();
    }
    res.redirect('/admin/login');
});
router.get('/', (req, res) => {
    res.render('admin/index');
});

router.get(['/profile', '/profile/edit/', '/profile/change-password/'], AuthControllers.getProfile);
router.post('/profile/edit/', ValidateHelpers.checkProfile, AuthControllers.changeInfo);
router.post('/profile/change-password/', ValidateHelpers.checkPassword, AuthControllers.changePassword);

router.get('/logout', AuthControllers.logout);


module.exports = router;