const {
    validationResult,
} = require('express-validator/check');
const bcrypt = require('bcryptjs');
const HandleResponse = require('../../../helpers/handleResponse');
const AuthRequests = require('../requests/admin');
const AuthRepositories = require('../repositories/admin');

module.exports = {
    verifyUser: async (user) => {
        const dbUser = await AuthRepositories.findUserById(user.id);
        if (!dbUser) return false;
        const {
            status,
            role,
        } = dbUser;
        if (status !== user.status || role !== user.role) return false;
        return true;
    },
    register: async (req, res) => {
        const {
            body,
        } = req;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const errMsg = AuthRequests.handleValidateRegister(errors);
            return AuthRequests.handleErrorRegister(body, errMsg, res);
        }
        AuthRepositories.saveUser(body).then(() => {
            req.flash('success', 'Đăng kí thành công. Vui lòng đăng nhập');
            res.redirect('/admin/login');
        });
    },
    login: async (req, res) => {
        const {
            body,
        } = req;
        const user = await AuthRepositories.findUser(body.username, body.username);
        if (!user) {
            const erMsg = { errUsername: 'Tên đăng nhập hoặc email không tồn tại' };
            return AuthRequests.handleErrorLogIn(body, erMsg, res);
        }
        if (!bcrypt.compareSync(body.password, user.password)) {
            const errMsg = { errPassword: 'Mật khẩu không chính xác' };
            return AuthRequests.handleErrorLogIn(body, errMsg, res);
        }
        switch (user.status) {
            case 'pending': {
                const errMsg = { errPassword: 'Tài khoản chưa được xác thực. Vui lòng liên hệ quản trị viên' };
                return AuthRequests.handleErrorLogIn(body, errMsg, res);
            }
            case 'suspended': {
                const errMsg = { errPassword: 'Tài khoản đã bị từ chối bởi quản trị viên' };
                return AuthRequests.handleErrorLogIn(body, errMsg, res);
            }
        }
        req.session.user = {
            id: user._id,
            avatar: user.avatar,
            fullname: user.fullname,
            username: user.username,
            phoneNumber: user.phoneNumber,
            email: user.email,
            role: user.role,
            status: user.status,
        };
        req.flash('success', `Chào mừng ${req.session.user.fullname}`);
        return res.redirect('/admin');
    },
    logout: async (req, res) => {
        try {
            await req.session.destroy();
            res.redirect('/admin/login');
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getProfile: async (req, res) => {
        try {
            return AuthRequests.renderProfile(req.session.user, res);
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    changeInfo: async (req, res) => {
        try {
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = AuthRequests.handleValidateEditProfile(errors);
                return AuthRequests.handleErrorEditProfile(body, errMsg, res);
            }
            const profile = {};
            if (body.avatar !== req.session.user.avatar) profile.avatar = body.avatar;
            if (body.fullname !== req.session.user.fullname) profile.fullname = body.fullname;
            if (body.email !== req.session.user.email) {
                const checkUser = await AuthRepositories.findUser(body.email, body.email);
                if (checkUser) {
                    const errMsg = { errEmail: 'Email đã tồn tại' };
                    return AuthRequests.handleErrorEditProfile(body, errMsg, res);
                }
                profile.email = await body.email;
            }
            await AuthRepositories.updateUser(req.session.user.id, profile);
            const user = await AuthRepositories.findUserById(req.session.user.id);
            req.session.user = {
                id: user._id,
                avatar: user.avatar,
                fullname: user.fullname,
                username: user.username,
                phoneNumber: user.phoneNumber,
                email: user.email,
                role: user.role,
                status: user.status,
            };
            req.flash('success', 'Sửa thông tin tài khoản thành công');
            res.redirect('/admin/profile');
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    changePassword: async (req, res) => {
        try {
            const {
                body,
            } = req;
            const user = await AuthRepositories.findUserById(req.session.user.id);
            if (!bcrypt.compareSync(body.oldPassword, user.password)) {
                const errMsg = { errOldPassword: 'Sai mật khẩu' };
                return AuthRequests.handleErrorChangePassword(req.session.user, errMsg, res);
            }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = AuthRequests.handleValidatePassword(errors);
                return AuthRequests.handleErrorChangePassword(req.session.user, errMsg, res);
            }
            if (bcrypt.compareSync(body.password, user.password)) {
                const errMsg = { errOldPassword: 'Mật khẩu chưa được thay đổi' };
                return AuthRequests.handleErrorChangePassword(req.session.user, errMsg, res);
            }
            const salt = await bcrypt.genSalt(10);
            const password = await bcrypt.hash(body.password, salt);
            const newPassword = { password };
            await AuthRepositories.updateUser(req.session.user.id, newPassword);
            req.flash('success', 'Đổi mật khẩu thành công');
            res.redirect('/admin/profile');
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
};