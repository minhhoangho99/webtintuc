const User = require('../../../models/user');
const Post = require('../../../models/post');
const Category = require('../../../models/category');
const Tag = require('../../../models/tag');

module.exports = {
    deletePostsByAuthor: async (author) => {
        const posts = await Post.find({
            author,
        }).select('_id');
        await Promise.all(posts.map(async (e) => {
            await Post.findByIdAndRemove(e._id);
            return true;
        }));
    },
    deleteCategoriesByAuthor: async (author) => {
        const categories = await Category.find({
            author,
        }).select('_id');
        const admin = await User.findOne({ role: 'admin' }).select('_id');
        await Promise.all(categories.map(async (e) => {
            const posts = await Post.find({ category: e._id }).select('_id');
            if (posts.length > 0) await Category.findByIdAndUpdate(e._id, { author: admin._id });
            else await Category.findByIdAndRemove(e._id);
            return true;
        }));
    },
    deleteTagsByAuthor: async (author) => {
        const tags = await Tag.find({
            author,
        }).select('_id');
        const admin = await User.findOne({ role: 'admin' }).select('_id');
        await Promise.all(tags.map(async (e) => {
            const posts = await Post.find({ tags: e._id }).select('_id');
            if (posts.length > 0) await Tag.findByIdAndUpdate(e._id, { author: admin._id });
            else await Tag.findByIdAndRemove(e._id);
            return true;
        }));
    },

    countDocuments: filter => User.countDocuments(filter),
    updateUser: (id, field) => User.findByIdAndUpdate(id, field, {
        new: true,
    }),
    getUsersOnPage: (perPage, page) => User.find({
            deletedAt: null,
            status: 'active',
        })
        .select('_id fullname username email phoneNumber role createdAt')
        .sort({
            updatedAt: -1,
        })
        .skip((perPage * page) - perPage)
        .limit(perPage),
    getInactiveUsers: (perPage, page) => User.find({
            deletedAt: null,
            status: { $in: ['pending', 'suspended'] },
        })
        .select('_id fullname username email phoneNumber status createdAt')
        .sort({
            updatedAt: -1,
        })
        .skip((perPage * page) - perPage)
        .limit(perPage),
    getUsersOnBin: (perPage, page) => User.find({
            deletedAt: {
                $ne: null,
            },
        })
        .select('fullname username email phoneNumber role createdAt deletedAt')
        .sort({
            updatedAt: -1,
        })
        .skip((perPage * page) - perPage)
        .limit(perPage),
    deleteUser: id => User.findByIdAndRemove(id),
};