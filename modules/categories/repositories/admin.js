const Category = require('../../../models/category');
const Post = require('../../../models/post');

module.exports = {
    findPostsByCategory: id => Post.find({
        category: id,
    }).select('_id category'),
    findCategoryById: id => Category.findById(id),

    findCategoryByUrl: url => Category.findOne({
        url,
    }),
    findGrandparentCategories: () => Category.find({
        parentId: '0',
        grandparentId: '0',
        deletedAt: null,
    }).select('-__v'),
    findCategoriesByParentId: parentId => Category.find({
        parentId,
        deletedAt: null,
    }).select('-__v'),
    findChildCategories: () => Category.find({
        parentId: {
            $ne: '0',
        },
        grandparentId: {
            $ne: '0',
        },
        deletedAt: null,
    }),
    countDocuments: filter => Category.countDocuments(filter),

    categoryType: (category) => {
        if (category.grandparentId === '0' && category.parentId === '0') return 'grandparentCate';
        if (category.grandparentId === '0' && category.parentId !== '0') return 'parentCate';
        if (category.grandparentId !== '0') return 'childCate';
    },
    saveCategory: (body) => {
        const {
            name,
            author,
            parentId,
            grandparentId,
        } = body;
        if (parentId === '0') {
            const cCategory = new Category({
                name,
                parentId: grandparentId,
                author,
            });
            cCategory.save();
        } else {
            const cCategory = new Category({
                name,
                parentId,
                grandparentId,
                author,
            });
            cCategory.save();
        }
        return true;
    },
    updateCategory: async (id, field) => {
        const dbCate = await Category.findById(id);
        dbCate.set(field);
        return dbCate.save();
    },

    findCategoriesOnPage: () => Category
        .find({
            deletedAt: null,
        })
        .sort({
            name: 1,
        })
        .select('name parentId grandparentId createdAt')
        .populate('author', 'fullname'),
        // .skip((perPage * page) - perPage)
        // .limit(perPage),

    findCategoriesOnBin: () => Category
        .find({
            deletedAt: {
                $ne: null,
            },
        })
        .sort({
            name: 1,
        })
        .select('name parentId grandparentId createdAt deletedAt')
        .populate('author', 'fullname'),
        // .skip((perPage * page) - perPage)
        // .limit(perPage),
    deleteCategory: id => Category.findByIdAndRemove(id),
};