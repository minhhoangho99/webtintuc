module.exports = {
    handleValidateCategory: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
                case 'name':
                    if (!errMsg.errName) errMsg.errName = e.msg;
                    break;
            }
        });
        return errMsg;
    },
    handleSuccessCategories: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/category/manage', {
                categories: args[0],
                pagination: args[1],
                active: 'category_manage',
            });
        }
        return args[0].redirect('/admin/category/manage');
    },
    handleDeleteCategories: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/category/recycle_bin', {
                categories: args[0],
                pagination: args[1],
                active: 'category_recycle_bin',
            });
        }
        return args[0].redirect('/admin/category/bin');
    },
    handleErrorAddCategory: (body, errMsg, res) => {
        res.render('admin/body/category/add', {
            ...body,
            ...errMsg,
            active: 'category_add',
        });
    },
    handleErrorEditCategory: (id, body, errMsg, res) => {
        res.render('admin/body/category/edit', {
            id,
            ...body,
            ...errMsg,
            active: 'category_edit',
        });
    },
    renderEditCategory: (category, res) => {
        const id = category._id;
        const {
            name,
            parentId,
            grandparentId,
            grandparentCategoryList,
            parentCategoryList,
        } = category;
        res.render('admin/body/category/edit', {
            id,
            name,
            parentId,
            grandparentId,
            grandparentCategoryList,
            parentCategoryList,
            active: 'category_manage',
        });
    },
};