module.exports = {
    renderHomepage: (data, res) => {
        const {
            newArticles,
            popularArticles,
            selectedArticles,
            selected1Articles,
            selected2Articles,
            sidebarArticles1,
            sidebarArticles2,
            breakingNews,
            category,
        } = data;
        res.render('client/index', {
            newArticles,
            popularArticles,
            selectedArticles,
            selected1Articles,
            selected2Articles,
            sidebarArticles1,
            sidebarArticles2,
            breakingNews,
            category,
        });
    },
    renderSinglePost: (data, res) => {
        const {
            article,
            relatedPost,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
        } = data;
        res.render('client/single-post', {
            article,
            relatedPost,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
        });
    },
    renderPostCategory: (data, res) => {
        const {
            article,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
            pagination,
        } = data;
        res.render('client/categories-post', {
            article,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
            pagination,
        });
    }

}