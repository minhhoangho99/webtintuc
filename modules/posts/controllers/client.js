
const RedisStorage = require('../../../helpers/reidisStorage');
const PostRepositories = require('../repositories/client');
const CategoryRepositories = require('../../categories/repositories/client');
const PageRequest = require('../requests/client');
const HandleResponse = require('../../../helpers/handleResponse');
const Config = require('../../../config/config');
const Pagination = require('../../../helpers/pagination');


const getPostByUrl = async (req, res) => {
    try {
        const { url } = req.params;
        const category = await RedisStorage.getRenderCategory(); 
        const article = await PostRepositories.PostByUrl(url);
        const relatedPost = await PostRepositories.RelatedPosts(article.category);
        const popularArticles = await PostRepositories.PostsByViews(5);
        const sidebarArticles1 = await PostRepositories.sideBarPosts(5);
        const sidebarArticles2 = await PostRepositories.sideBarPosts(4);
        const views = article.views + 1;
        await PostRepositories.UpdateView(url, views);
        const data = {
            article,
            relatedPost,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
        };
        return PageRequest.renderSinglePost(data, res);
    } catch (err) {
        err.status = 500;
        HandleResponse.handleErrorClient(500, res);
    }
};

const getPostsByCategory = async (req, res) => {
    try {
        const category = await RedisStorage.getRenderCategory();
        const { url } = req.params;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const { perPage } = Config;

        const listCate = await CategoryRepositories.getAllChildByUrl(url);
        const fullArticle = await PostRepositories.PostByCategory(listCate, 10000);
        const count = fullArticle.length;
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorClienta(404, res);
        }
        const article = await PostRepositories.PostByCategoryOnPage(listCate, 5, page);

        const pagination = await Pagination.renderPagination(page, countPage, "/" + url);

        const sidebarArticles1 = await PostRepositories.sideBarPosts(5);
        const sidebarArticles2 = await PostRepositories.sideBarPosts(5);
        const popularArticles = await PostRepositories.PostsByViews(5);
        const data = {
            article,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
            pagination,
        };
        return PageRequest.renderPostCategory(data, res);
    } catch (err) {
        err.status = 500;
        HandleResponse.handleErrorClient(500, res);
    }
};
const getArticles = async (req, res) => {
    try {
        const category = await RedisStorage.getRenderCategory();
        const newArticles = await PostRepositories.newPosts(3);
        const popularArticles = await PostRepositories.PostsByViews(5);
        const breakingNews = await PostRepositories.GetBreakingNews(4);
        const sidebarArticles1 = await PostRepositories.sideBarPosts(5);
        const sidebarArticles2 = await PostRepositories.sideBarPosts(4);
        //
        let url = category[0].url;
        let listCate = await CategoryRepositories.getAllChildByUrl(url);
        const selectedArticles = await PostRepositories.selectedPosts(listCate, 3);
        //
        url = category[2].url;
        listCate = await CategoryRepositories.getAllChildByUrl(url);
        const selected1Articles = await PostRepositories.selectedPosts(listCate, 3);

        url = category[1].url;
        listCate = await CategoryRepositories.getAllChildByUrl(url);
        const selected2Articles = await PostRepositories.selectedPosts(listCate, 3);
        ///get sidebar view
        const data = {
            newArticles,
            popularArticles,
            selectedArticles,
            selected1Articles,
            selected2Articles,
            sidebarArticles1,
            sidebarArticles2,
            breakingNews,
            category,
        }
        return PageRequest.renderHomepage(data, res);
    } catch (error) {
        error.status = 500;
        HandleResponse.handleErrorClient(500, res);
    }
};

module.exports = {
    getArticles,
    getPostByUrl,
    getPostsByCategory,
};
