const Post = require('../../../models/post');
const Category = require('../../../models/category');
const Tag = require('../../../models/tag');

module.exports = {
    //new Post
    newPosts: limit => Post.find({
            deletedAt: null,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            updatedAt: -1,
        })
        .select('title description content createdAt url cover')
        .limit(limit),
    // Popular -- view
    PostsByViews: limit => Post.find({
            deletedAt: null,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            views: -1,
        })
        .select('title description content createdAt url cover')
        .limit(limit),
    // get sidebar posts
    sideBarPosts: limit => Post.find({
            deletedAt: null,
        })
        .populate('author', 'fullname')
        .populate('category', 'name')
        .sort({
            updatedAt: -1,
        })
        .select('title createdAt url cover')
        .limit(limit),

    //getSelectedPost
    selectedPosts: listCategory  => Post.find({
            deletedAt: null,
            category: { $in: listCategory },
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            createdAt: -1,
        })
        .select('title description content createdAt url cover')
        .limit(3),
    //post by category
    PostByCategory: (listCategory, limit)  => Post.find({
            deletedAt: null,
            category: { $in: listCategory },
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            createdAt: -1,
        })
        .select('title description content createdAt url cover')
        .limit(limit),
    PostByCategoryOnPage: (listCategory, perPage, page) => Post
        .find({
            deletedAt: null,
            category: { $in: listCategory },
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            createdAt: -1,
        })
        .select('title description content createdAt url cover')
        .skip((perPage * page) - perPage)
        .limit(perPage),
    //outstanding news
    GetBreakingNews: limit => Post.find({
            deletedAt: null,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .sort({
            updatedAt: -1,
        })
        .select('title description content createdAt url cover')
        .skip(3)
        .limit(limit),
    //post by url
    PostByUrl: url => Post
        .findOne({ url })
        .populate('tags', 'name')
        .populate('category', 'name')
        .populate('author', '-_id fullname'),

    RelatedPosts: cate => Post
        .find({ category: cate })
        .populate('category', 'name,url')
        .sort({
            updatedAt: -1,
        }).limit(2),
    UpdateView: (url, views) => Post
        .findOneAndUpdate({
            url,
        }, {
            views,
        }, {
            new: true,
        }),
    
}