const Post = require('../../../models/post');
const Category = require('../../../models/category');
const Tag = require('../../../models/tag');

module.exports = {
    findGrandparentCategories: () => Category.find({
        parentId: '0',
        grandparentId: '0',
        deletedAt: null,
    }).sort({ name: 1 }).select('-__v'),
    findCategoriesByParentId: parentId => Category.find({
        parentId,
        deletedAt: null,
    }).sort({ name: 1 }).select('-__v'),
    findCategoryByChildId: async (childId) => {
        const childCate = await Category.findById(childId).select('parentId');
        return Category.findById(childCate.parentId);
    },
    findCategoryById: id => Category.findById(id),
    categoryType: (category) => {
        if (category.grandparentId === '0' && category.parentId === '0') return 'grandparentCate';
        if (category.grandparentId === '0' && category.parentId !== '0') return 'parentCate';
        if (category.grandparentId !== '0') return 'childCate';
    },
    findTags: () => Tag.find({
        deletedAt: null,
    }).sort({ name: 1 }).select('_id name'),


    savePost: (body) => {
        const {
            title,
            description,
            content,
            author,
            childId,
            parentId,
            grandparentId,
            tags,
            cover,
        } = body;
        let category;
        if (grandparentId !== '0' && parentId === '0' && childId === '0') category = grandparentId;
        if (grandparentId !== '0' && parentId !== '0' && childId === '0') category = parentId;
        if (childId !== '0') category = childId;

        const cPost = new Post({
            title,
            description,
            content,
            author,
            category,
            tags,
            cover,
        });
        return cPost.save();
    },
    countDocuments: filter => Post.countDocuments(filter),
    findPostById: id => Post.findById(id).select('-__v'),
    findPostByUrl: url => Post.findOne({
        url,
    }),

    updatePost: async (id, field) => {
        const {
            title,
            description,
            content,
            author,
            childId,
            parentId,
            grandparentId,
            tags,
            cover,
        } = field;
        let category;
        if (grandparentId !== '0' && parentId === '0' && childId === '0') category = grandparentId;
        if (grandparentId !== '0' && parentId !== '0' && childId === '0') category = parentId;
        if (childId !== '0') category = childId;
        const post = {
            title,
            description,
            content,
            author,
            category,
            tags,
            cover,
        };
        const dbPost = await Post.findById(id);
        dbPost.set(post);
        return dbPost.save();
    },
    updateDeletedAtPost: (id, field) => Post.findByIdAndUpdate(id, field, {
        new: true,
    }),
    getPostsOnPage: (perPage, page) => Post.find({
            deletedAt: null,
        })
        .sort({
            updatedAt: -1,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .select('title description content createdAt updatedAt url views')
        .skip((perPage * page) - perPage)
        .limit(perPage),
    getPostsOnPageByAuthor: (authorId, perPage, page) => Post.find({
            author: authorId,
            deletedAt: null,
        })
        .sort({
            updatedAt: -1,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .select('title description content createdAt updatedAt url views')
        .skip((perPage * page) - perPage)
        .limit(perPage),
    getPostsOnBin: (perPage, page) => Post.find({
            deletedAt: {
                $ne: null,
            },
        })
        .sort({
            updatedAt: -1,
        })
        .populate('author', 'fullname')
        .populate('tags', 'name')
        .populate('category', 'name')
        .select('title description content createdAt updatedAt deletedAt url views')
        .skip((perPage * page) - perPage)
        .limit(perPage),
    deletedPost: id => Post.findByIdAndRemove(id),
};