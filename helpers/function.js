const moment = require('moment');
const limit = require('limit-string-length');

moment.locale('vi');

module.exports = {
    checkFullname: (name) => {
        const x = /(^[a-zaáàảãạâấầẩẫậăắằẳẵặeéèẻẽẹêếềểễệiíìỉĩịoóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵđ]+)( [a-zaáàảãạâấầẩẫậăắằẳẵặeéèẻẽẹêếềểễệiíìỉĩịoóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵđ]+)+$/i;
        return x.test(name);
    },
    checkName: (name) => {
        const x = /(^[a-zaáàảãạâấầẩẫậăắằẳẵặeéèẻẽẹêếềểễệiíìỉĩịoóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵđ ]+)+$/i;
        return x.test(name);
    },
    checkUsername: (username) => {
        const x = /^(\w)+$/i;
        return x.test(username);
    },
    checkPhoneNumber: (phoneNumber) => {
        const x = /^(0|\+84|84)(16[^01][0-9]{7}$|12[0-9]{8}$|18[68][0-9]{7}$|19[9][0-9]{7}$|8[689][0-9]{7}$|9[^5][0-9]{7}$)/;
        return x.test(phoneNumber);
    },
    checkPassword: (password) => {
        const x = /^([\w!@#$%^&*()-_+={}|:;"'<>,./? ])+$/i;
        return x.test(password);
    },

    getTime: function getTime(time) {
        return moment(time).fromNow();
    },
    getTimeDetail: function getTimeDetail(time) {
        return moment(time).format('HH:mm DD/MM/YYYY');
    },
    getLim: function getLim(lim) {
        return limit(lim, 300);
    },
};