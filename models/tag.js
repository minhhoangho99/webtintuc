const mongoose = require('mongoose');

const {
    Schema,
} = mongoose;


const getSlug = require('speakingurl');

const tagSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
    },
    url: {
        type: String,
        unique: true,
        trim: true,
    },
    deletedAt: {
        type: Date,
        default: null,
    },
}, {
    timestamps: true,
});

tagSchema.pre('save', function creatURL(next) {
    const tag = this;
    if (!tag.isModified('name')) return next();
    const url = getSlug(tag.name, {
        lang: 'vn',
    });
    tag.url = url;
    next();
});
module.exports = mongoose.model('tags', tagSchema);
