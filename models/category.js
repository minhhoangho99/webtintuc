const mongoose = require('mongoose');

const {
    Schema,
} = mongoose;
const getSlug = require('speakingurl');


const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    parentId: {
        type: String,
        trim: true,
        default: '0',
    },
    grandparentId: {
        type: String,
        trim: true,
        default: '0',
    },
    deletedAt: {
        type: Date,
        default: null,
    },
    url: {
        type: String,
        unique: true,
        trim: true,
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
    },
}, {
    timestamps: true,
});

categorySchema.pre('save', function createURL(next) {
    const category = this;
    if (!category.isModified('name')) return next();
    const url = getSlug(category.name, {
        lang: 'vn',
    });
    category.url = url;
    next();
});

module.exports = mongoose.model('categories', categorySchema);
