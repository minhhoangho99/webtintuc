const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const {
    Schema,
} = mongoose;
const userSchema = new Schema({
    fullname: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        unique: true,
        lowercase: true,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true,
    },
    phoneNumber: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    avatar: {
        type: String,
        default: 'https://images.vexels.com/media/users/3/129733/isolated/preview/a558682b158debb6d6f49d07d854f99f-casual-male-avatar-silhouette-by-vexels.png',
    },
    role: {
        type: String,
        default: 'poster',
        required: true,
    },
    access: {
        type: String,
        default: null,
    },
    status: {
        type: String,
        default: 'pending',
    },
    activationCode: {
        type: String,
        default: null,
    },
    deletedAt: {
        type: Date,
        default: null,
    },
}, {
    timestamps: true,
});

userSchema.pre('save', function sanitization(next) {
    const user = this;
    if (!user.isModified('password')) return next();
    bcrypt.genSalt(10, (err1, salt) => {
        if (err1) return next();
        bcrypt.hash(user.password, salt, (err2, hash) => {
            if (err2) return next();
            user.password = hash;
            next();
        });
    });
    if (user.phoneNumber.slice(0, 3) === '+84') {
        user.phoneNumber = user.phoneNumber.replace('+84', '0');
    }
    if (user.phoneNumber.slice(0, 2) === '84') {
        user.phoneNumber = user.phoneNumber.replace('84', '0');
    }
});
module.exports = mongoose.model('users', userSchema);
